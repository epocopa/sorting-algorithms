CC=gcc
CFLAGS=-Wall
FILES=launcher.c selection.c radix.c shell.c comb.c cycle.c quicksort.c print.c

all:
	$(CC) $(CFLAGS) -o launcher $(FILES)

clean:
	rm -rf launcher
