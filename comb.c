#include "comb.h"

void sort_comb(int a[], int length) {
	int sorted = 0, temp, gap = length;
	float shrink = 1.3;

	for (int i = 0; i < length && (sorted == 0 || gap > 1); i++) {
		sorted = 1;
		gap /= shrink;
		if (gap < 1) {
			gap = 1;
		}
		for (int j = 0; j + gap <= length - 1; j++) {
			if (a[j] > a[j + gap]) {
				temp = a[j];
				a[j] = a[j + gap];
				a[j + gap] = temp;
				sorted = 0;
			}
		}
	}
}
