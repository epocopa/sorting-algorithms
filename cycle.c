#include "cycle.h"

void sort_cycle(int a[], int length) {
	int item, pos, temp;
	for (int i = 0; i < length - 1; i++) {
		item = a[i];
		pos = i;
		//find correct position
		for (int j = i + 1; j < length; j++) {
			if (a[j] < item) {
				pos++;
			}
		}
		//only if item is out of place
		if (pos != i) {
			//avoid duplicates
			while (item == a[pos]) {
				pos++;
			}
			//swap elements
			temp = a[pos];
			a[pos] = item;
			item = temp;
			

			while (pos != i) {
				pos = i;

				for (int j = i + 1; j < length; j++) {
					//find correct position
					if (a[j] < item) {
						pos++;
					}
				}
				//avoid duplicates
				while (item == a[pos]) {
					pos++;
				}

				//swap elements
				temp = a[pos];
				a[pos] = item;
				item = temp;
				
			}
		}
	}
}
