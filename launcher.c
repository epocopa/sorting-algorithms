#include <stdio.h>
#include <stdlib.h>
#include "selection.h"
#include "radix.h"
#include "shell.h"
#include "comb.h"
#include "cycle.h"
#include "quicksort.h"

int options();
void launch(int op);
void testAll();
void generateRandomList(int *a, int length);
void checkOrder(int *a, int length);

int main(int argc, char const *argv[]) {
	int option = options();
	launch(option);
	return 0;
}

int options(){
	int option;
	do {
		printf(" Welcome to The Algorithms Launcher  \n\n"
		"\t 0 - Selection sort (Easy)\n"
		"\t 1 - Radix sort (Easy)\n"
		"\t 2 - Shell sort (Moderate)\n"
		"\t 3 - Comb sort (Moderate)\n"
		"\t 4 - Cycle sort (Moderate)\n"
		"\t 5 - Quicksort (Hard)\n"
		"\t 6 - Test all with One Hundred Thousand numbers\n\n"
		" Select an option: ");

		scanf("%d", &option);
	} while(option < 0 || option > 6 );
	return option;
}

void launch(int op) {
	if (op == 6) {
		testAll();
		return;
	}

	int a[] = {0, -3, 10, 3, 1, 4, 7, 1};
	int length = sizeof(a) / sizeof(a[0]);

	printf("\nOriginal List: \n");
	print(a, length);
	printf("\n");

	switch (op) {
		case 0:
			sort_selection(a, length);
			break;
		case 1:
			sort_radix(a, length);
			break;
		case 2:
			sort_shell(a, length);
			break;
		case 3:
			sort_comb(a, length);
			break;
		case 4:
			sort_cycle(a, length);
			break;
		case 5:
			sort_quicksort(a, 0, length - 1);
			break;
	}
	printf("\n Sorted List: \n");
	print(a, length);
}

void testAll() {
	int length = 100000;
	int *a = malloc(length *sizeof(int));

	generateRandomList(a, length);
	printf("Testing Selection sort...\n");
	sort_selection(a, length);
	checkOrder(a, length);

	generateRandomList(a, length);
	printf("Testing Radix sort...\n");
	sort_radix(a, length);
	checkOrder(a, length);

	generateRandomList(a, length);
	printf("Testing Shell sort...\n");
	sort_shell(a, length);
	checkOrder(a, length);

	generateRandomList(a, length);
	printf("Testing Comb sort...\n");
	sort_comb(a, length);
	checkOrder(a, length);

	generateRandomList(a, length);
	printf("Testing Cycle sort...\n");
	sort_cycle(a, length);
	checkOrder(a, length);

	generateRandomList(a, length);
	printf("Testing Quicksort...\n");
	sort_quicksort(a, 0, length - 1);
	checkOrder(a, length);
}

void generateRandomList(int *a, int length) {
	printf("\nGenerating random list...\n");
	for (int i = 0; i < length; i++) {
		a[i] = rand() % 1000  - i;
	}
}

void checkOrder(int *a, int length) {
	int order = 0;
	for (int i = 0; i < length - 1; i++) {
			if (a[i] > a[i + 1]) {
			order = 1;
			}
	}
	if (order) {
		printf("The list is not sorted\n");
	} else {
		printf("The list is sorted\n");
	}
}
