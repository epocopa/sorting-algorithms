#include "print.h"

void print(int a[], int length) {
	for (int i = 0; i < length; i++) {
		printf("%2d ", a[i]);
	}
	printf("\n");
}
