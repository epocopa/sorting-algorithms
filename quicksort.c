#include "quicksort.h"

void sort_quicksort(int a[],int start, int end) {
	if (start >= end) {
		return;
	}
	int pivot = partition(a, start, end);
	sort_quicksort(a, start, pivot);
	sort_quicksort(a, pivot + 1, end);
}

int partition(int a[], int start, int end) {
	int pivot = start;
	int left = start - 1;
	int right = end + 1;
	int temp;

	while (1) {
		do {
			left++;
		} while(a[left] < a[pivot]);

		do {
			right--;
		} while(a[right] > a[pivot]);

		if (left >= right) {
			return right;
		} else {
			temp = a[left];
			a[left] = a[right];
			a[right] = temp;
		}
	}
}
