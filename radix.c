#include "radix.h"

void sort_radix(int a[], int length){
	//sort negatives first, allocation of length just in case...
	int negatives[length], positives[length];
	int countN = 0, countP = 0;
	//Store in respectives arrays
	for (size_t i = 0; i < length; i++) {
		if (a[i] < 0) {
			negatives[countN++] = -a[i];
		} else{
			positives[countP++] = a[i];
		}
	}

	sort(negatives, countN);
	for (int i = 0; i < countN; i++) {
		a[i] = -negatives[countN-i-1];
	}

	sort(positives, countP);
	for (int i = 0; i < countP; i++) {
		a[countN+i] = positives[i];
	}

}

void sort(int a[], int length) {
	// Find max to know number of digits
	int max = getMax(a, length);
	for (int exp = 1; max / exp > 0; exp *= 10) {
		int out[length];
		int bucket[10] = {0};



		for (int j = 0; j < length; j++) {
			bucket[(a[j]/exp) % 10]++;
		}

		//Add the count of the previous buckets,
		for (int j = 1; j < 10; j++) {
			bucket[j] += bucket[j-1];
		}

		for (int j = length - 1; j >= 0; j--) {
			out[--bucket[ (a[j] / exp) % 10]] = a[j];
		}

		for (int j = 0; j < length; j++) {
			a[j] = out[j];
		}
	}

}


int getMax(int a[], int length) {
	int max = a[0];
	for (int i = 1; i < length; i++) {
		if (a[i] > max) {
			max = a[i];
		}
	}
	return max;
}
