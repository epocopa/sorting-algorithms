#include "selection.h"

void sort_selection(int a[], int length) {
	int min, temp;

	for (int i = 0; i < length; i++) {
		min = i;
		for (int j = i; j < length; j++) {
			if (a[j] < a[min]) {
				min = j;
			}
		}
		temp = a[i];
		a[i] = a[min];
		a[min] = temp;
	}
}
