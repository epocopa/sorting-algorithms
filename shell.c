#include "shell.h"

void sort_shell(int a[], int length) {
	//there is a lot of discussion going aorund as wich is the best gap,
	// I'll just stick to n/2
	int temp;
	for (int gap = length/2; gap > 0; gap /= 2) {
		for (int i = gap; i < length; i++) {
			temp = a[i];
			int j = i;
			while (j >= gap && a[j - gap] > temp) {
				a[j] = a[j - gap];
				 j -= gap;
			}
			a[j] = temp;

		}
	}
}
